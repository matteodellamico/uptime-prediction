## Definition of a few useful functions

logistic = function(a)
  {
    1 / (1 + exp(-a))
  }

LOGSUM = function(a, b)
{
  if(min(a) >= min(b)) return(a + log(1 + exp(b-a)))
  b + log(1 + exp(a-b))
}

LOGDIFF = function(ab)
{
 ab[1] + log(1 - exp(ab[2]-ab[1]))
}

LOGSUM.VECT = function(a)
{
  max.a = max(a)
  max.a + log(sum(exp(a - max.a)))
}

log.logistic = function(a)
  {
    -LOGSUM(-a, 0)
  }


## **************************************************************************************************** 
## ************************************************** LOG PRIOR
## **************************************************************************************************** 

fun.log.p.beta = function(beta.vect, m0, S0)
  {
    -0.5 * t(beta.vect - m0) %*% solve(S0) %*% (beta.vect - m0)
  }

## **************************************************************************************************** 
## ************************************************** LOG LIKELIHOOD
## **************************************************************************************************** 

fun.log.p.y.giv.beta = function(X, y, beta.vect)
  {
    a = X %*% beta.vect
    log.logistic.vect = log.logistic(a)
    log.logistic.vect.minus = log.logistic(-a)
    res = sum(y * log.logistic.vect + (1 - y) * log.logistic.vect.minus)

    if(is.nan(res)) res = -Inf
    res
  }

fun.log.joint = function(X, y, beta.vect, m0, S0)
  {
    fun.log.p.y.giv.beta(X, y, beta.vect) + fun.log.p.beta(beta.vect, m0, S0)
  }

## **************************************************************************************************** 
## ************************************************** GRADIENT
## **************************************************************************************************** 

fun.grad.log.joint = function(X, y, beta.vect, m0, S0)
  {
    a = X %*% beta.vect
    log.logistic.vect = log.logistic(a)

    t(X) %*% (y - exp(log.logistic.vect)) - solve(S0) %*% (beta.vect - m0)
  }

## **************************************************************************************************** 
## ************************************************** NEGATIVE HESSIAN
## **************************************************************************************************** 

fun.neg.hess.log.joint = function(X, y, beta.vect, m0, S0)
  {
    a = X %*% beta.vect
    log.logistic.vect = log.logistic(a)
    log.logistic.vect.minus = log.logistic(-a)

    V = diag(exp(c(log.logistic.vect + log.logistic.vect.minus)))

    t(X) %*% V %*% X + solve(S0)
  }


## **************************************************************************************************** 
## ************************************************** UPDATE PRIOR
## **************************************************************************************************** 

update.prior = function(X, y, m0, S0)
  {
    beta.vect = m0
    
    for(i in 1:50)
      {
        a = X %*% beta.vect
        log.logistic.vect = log.logistic(a)
        log.logistic.vect.minus = log.logistic(-a)

        diag.V = exp(c(log.logistic.vect + log.logistic.vect.minus))
        neg.hess = crossprod(X, diag.V * X) + solve(S0)

        grad = t(t(y - exp(log.logistic.vect)) %*% X) - solve(S0) %*% (beta.vect - m0)
        
        beta.prime = beta.vect + solve(neg.hess) %*% grad
        
        if(sum(fun.grad.log.joint(X, y, beta.prime, m0, S0)^2) < 1e-12) break
        beta.vect = beta.prime
      }
    
    list(beta.prime, solve(neg.hess))
  }

update.prior.slow = function(X, y, m0, S0)
  {
    beta.vect = m0
    
    for(i in 1:50)
      {
        beta.prime = beta.vect + solve(fun.neg.hess.log.joint(X, y, beta.vect, m0, S0)) %*% fun.grad.log.joint(X, y, beta.vect, m0, S0)
        
        if(sum(fun.grad.log.joint(X, y, beta.prime, m0, S0)^2) < 1e-12) break
        beta.vect = beta.prime
      }
    
    list(beta.prime, solve(fun.neg.hess.log.joint(X, y, beta.vect, m0, S0)))
  }

## **************************************************************************************************** 
## ************************************************** PREDICT
## **************************************************************************************************** 

predict.log.reg = function(xstar, beta.vect, S)
  {
    ma = t(xstar) %*% beta.vect
    s2a = t(xstar) %*% S %*% xstar

    tmp = (1 + pi * s2a / 8)^(-1/2)
    
    exp(log.logistic(tmp * ma))
  }


## **************************************************************************************************** 
## ************************************************** DATA GENERATION - FOR TESTING
## **************************************************************************************************** 

GENERATE.DATA = function(true.beta, n)
  {
    d = length(true.beta) - 1
    
    x.gr = cbind(rep(1, 2000), matrix(runif(2000*d), 2000, d))
    n.gr = dim(x.gr)[1]

    true.latent = x.gr %*% true.beta

    p.y.gr = logistic(true.latent)
    y.gr = rbinom(n.gr, 1, p.y.gr)

    set.seed(1)
    ind.to.take = sample(c(1:n.gr), n)
    X = matrix(x.gr[ind.to.take,], ncol=d+1)
    y = y.gr[ind.to.take]

    assign("TRUE.P", logistic(true.latent[ind.to.take]), .GlobalEnv)
    assign("TEST.TRUE.P", logistic(true.latent), .GlobalEnv)

    DATA$n = n
    DATA$d = d
    DATA$X = X
    DATA$y = y

    DATA$ntest = length(y.gr)
    DATA$X.TEST = x.gr
    DATA$y.TEST = y.gr
  }
