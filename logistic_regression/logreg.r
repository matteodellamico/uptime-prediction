source("utilities.r")

SET.FEATURES = c(1,2,3,4,6)

DATASET = "kad_mn"

base.dir.data = paste("~/DATA/", DATASET, "/", sep="")

N.USERS = as.integer(system(paste("wc -l ", base.dir.data, "B", " | perl -pe's/\\s+(\\d+)\\s+.+/$1/g'", sep=""), intern=T))
N.HOURS = as.integer(system(paste("head -1 ", base.dir.data, "B", "| wc -w", sep=""), intern=T))

D = length(SET.FEATURES)

filename.train.feat1 = paste(base.dir.data, "individual_daily_B", sep="")
filename.train.feat2 = paste(base.dir.data, "individual_flat_B", sep="")
filename.train.feat3 = paste(base.dir.data, "individual_weekly_B", sep="")
filename.train.feat4 = paste(base.dir.data, "global_daily_B", sep="")
filename.train.feat6 = paste(base.dir.data, "global_weekly_B", sep="")
filename.train.labels = paste(base.dir.data, "B", sep="")

filename.test.feat1 = paste(base.dir.data, "individual_daily_D", sep="")
filename.test.feat2 = paste(base.dir.data, "individual_flat_D", sep="")
filename.test.feat3 = paste(base.dir.data, "individual_weekly_D", sep="")
filename.test.feat4 = paste(base.dir.data, "global_daily_D", sep="")
filename.test.feat6 = paste(base.dir.data, "global_weekly_D", sep="")
filename.test.labels = paste(base.dir.data, "D", sep="")

m.prior = m0 = rep(0, D+1)
S.prior = S0 = diag(D+1) * 1e4


## ************************************************************************************************************************ Training
print("Training - Loading files")
X.train = matrix(1, ncol=1, nrow=N.USERS*N.HOURS)
for(ind.filename in SET.FEATURES)
  {
    print(paste("Loading", ind.filename))
    var2 = paste("filename.train.feat", ind.filename, sep="")
    X.train = cbind(X.train, c(as.matrix(read.table(get(var2)))))
  }

print("Loading labels")
X.labels = c(as.matrix(read.table(filename.train.labels)))

ind.to.balance = c(1:length(X.labels))

## Update the prior after observing a set of samples
print("Logistic regression")
res = update.prior(X.train[ind.to.balance,], X.labels[ind.to.balance], m0, S0)
m0 = res[[1]]
S0 = res[[2]]

cat("\n\n")

## ************************************************************************************************************************ Test
N.USERS = as.integer(system(paste("wc -l ", base.dir.data, "D", " | perl -pe's/\\s+(\\d+)\\s+.+/$1/g'", sep=""), intern=T))
N.HOURS = as.integer(system(paste("head -1 ", base.dir.data, "D", "| wc -w", sep=""), intern=T))

print("Test - Loading files")
X.test = matrix(1, ncol=1, nrow=N.USERS*N.HOURS)
for(ind.filename in SET.FEATURES)
  {
    print(paste("Loading", ind.filename))
    var2 = paste("filename.test.feat", ind.filename, sep="")
    X.test = cbind(X.test, c(as.matrix(read.table(get(var2)))))
  }

print("Loading labels")
TRUE.TEST.LABELS = c(as.matrix(read.table(filename.test.labels)))

print("Predicting")
ma = X.test %*% m0
tmp = (X.test %*% S0) * X.test
s2a = tmp %*% rep(1, D+1)
tmp = (1 + pi * s2a / 8)^(-1/2)
p.test.predicted = exp(log.logistic(tmp * ma))

confusion = table((p.test.predicted > 0.5), TRUE.TEST.LABELS)

## ************************************************************************************************************************************* Output results on files

if(length(SET.FEATURES) != 1)
  {
    write.table(matrix(p.test.predicted, N.USERS, length(p.test.predicted)/N.USERS), file=paste(base.dir.data, "predictions_D", sep=""), quote=F, row.names=F, col.names=F)
    cat(c(m0), "\n", file=paste(base.dir.data, "beta_mean", sep=""))
    write.table(S0, file=paste(base.dir.data, "beta_cov", sep=""), quote=F, row.names=F, col.names=F)
    
    system(paste("gzip ", base.dir.data, "predictions_D", sep=""))
  }

if(length(SET.FEATURES) == 1)
  {
    write.table(matrix(p.test.predicted, N.USERS, length(p.test.predicted)/N.USERS), file=paste(base.dir.data, "predictions_D_feat", SET.FEATURES, sep=""), quote=F, row.names=F, col.names=F)
    cat(c(m0), "\n", file=paste(base.dir.data, "beta_mean_feat", SET.FEATURES, sep=""))
    write.table(S0, file=paste(base.dir.data, "beta_cov_feat", SET.FEATURES, sep=""), quote=F, row.names=F, col.names=F)
    
    system(paste("gzip ", base.dir.data, "predictions_D_feat", SET.FEATURES, sep=""))
  }
