library(xtable)
library(ROCR)

SET.FEATURES = c(1,2,3,4,6)

DATASET = "gw"

base.dir.data = paste("~/DATA/", DATASET, "/", sep="")

N.USERS = as.integer(system(paste("wc -l ", base.dir.data, "D", " | perl -pe's/\\s+(\\d+)\\s+.+/$1/g'", sep=""), intern=T))
N.HOURS = as.integer(system(paste("head -1 ", base.dir.data, "D", "| wc -w", sep=""), intern=T))

D = length(SET.FEATURES)

filename.test.labels = paste(base.dir.data, "D", sep="")
filename.test.prediction.tot = paste(base.dir.data, "predictions_D", sep="")
filename.test.prediction1 = paste(base.dir.data, "predictions_D_feat1", sep="")
filename.test.prediction2 = paste(base.dir.data, "predictions_D_feat2", sep="")
filename.test.prediction3 = paste(base.dir.data, "predictions_D_feat3", sep="")
filename.test.prediction4 = paste(base.dir.data, "predictions_D_feat4", sep="")
filename.test.prediction6 = paste(base.dir.data, "predictions_D_feat6", sep="")

print("Loading files prediction")
pred.matrix = c(as.matrix(read.table(filename.test.prediction.tot)))
for(ind.filename in SET.FEATURES)
  {
    print(paste("Loading", ind.filename))
    var2 = paste("filename.test.prediction", ind.filename, sep="")
    pred.matrix = cbind(pred.matrix, c(as.matrix(read.table(get(var2)))))
  }

print("Loading labels")
TRUE.TEST.LABELS = c(as.matrix(read.table(filename.test.labels)))


table.results = matrix(0, 6, 2)
row.names(table.results) = c("ALL", "ind daily", "ind flat", "ind weekly", "glob daily", "glob weekly")
colnames(table.results) = c("AUC", "GM")

par(mfrow = c(3,3))
for(iii in 1:dim(pred.matrix)[2])
  {
    pred = prediction( pred.matrix[,iii], TRUE.TEST.LABELS)
    auc = performance(pred,"auc")@y.values[[1]]

    print(auc)
    
    geomean = exp(mean(TRUE.TEST.LABELS * log(pred.matrix[,iii]) + (1-TRUE.TEST.LABELS) * log(1-pred.matrix[,iii])))

    print(geomean)

    zzz = performance(pred,"tpr","fpr")
    plot(zzz); abline(0,1)

    table.results[iii,] = c(auc, geomean)
  }

xtable(table.results, digits=3)
