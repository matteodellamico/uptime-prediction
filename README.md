Repository for the source code of the experiments from
"On User Availability Predictions and Network Applications"
by Matteo Dell'Amico, Maurizio Filippone, Pietro Michiardi, and Yves Roudier
IEEE/ACM Transactions on Networking, 2014.

This source code is shared to help reproducibility. Apologies for the
lack of documentation: if clarifications are needed, please contact
the authors -- Matteo Dell'Amico for the part written in Python, and
Maurizio Filippone for the part written in R.