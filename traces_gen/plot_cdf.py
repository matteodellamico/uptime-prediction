from __future__ import division

import cPickle as pickle
import numpy as np
import matplotlib
import gzip
from matplotlib import pyplot as plt

matplotlib.rc('text', usetex=True)
matplotlib.rc('font', family='serif', size=20)
matplotlib.rc('legend', fontsize=20)
matplotlib.rc('figure', figsize=(8, 6))
#pyplot.figure().subplots_adjust(top=0.93, bottom=0.2)

def plotcdf(data, style, label=None):
    plt.plot(sorted(data), np.linspace(1 / len(data), 1, len(data)),
             style, label=label, lw=4)
    plt.xlabel("Per-user average availability")
    plt.ylabel("Empirical CDF")

datasets = [('imclean', 'IM', '-'),
            ('gw', 'GW', '--'),
            ('kad', 'Kad', ':')]

for ds, lab, style in datasets:
    fn = 'avail_{}_3600'.format(ds)
    try:
        M = pickle.load(gzip.open(fn + '.pickle.gz'))
    except IOError:
        M = np.memmap('/tmp/avail_kad_3600.dat',
                      dtype=bool, shape=(400375,4296))
    M = M[:, :24 * 7 * 18]
    plotcdf(M.mean(1), style, lab)
plt.grid()
plt.legend(loc=0)
plt.show()
    
    
