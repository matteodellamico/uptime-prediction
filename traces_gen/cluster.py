from __future__ import division

import cPickle as pickle
import numpy as np
import gzip
import sys
from scipy.cluster import vq

ds = sys.argv[1]
n = int(sys.argv[2])
            
fn = 'avail_{}_3600'.format(ds)
try:
    M = pickle.load(gzip.open(fn + '.pickle.gz'))
except IOError:
    M = np.memmap('/tmp/avail_kad_3600.dat',
                  dtype=bool, shape=(400375,4296))

result = vq.kmeans2(M.astype(int), n)
pickle.dump(result, gzip.open('clusters_{}_3600_{}.pickle.gz'.format(ds, n), 'w'))
