import gzip
import itertools
import sys

import numpy as np

def D():
    with gzip.open(sys.argv[1]) as f:
        for line in f:
            yield np.array([bool(float(v)) for v in line.strip().split()])

def pred():
    with gzip.open(sys.argv[2]) as f:
        for line in f:
            yield np.array([float(v) for v in line.strip().split()])

for i, (data, pred) in enumerate(itertools.izip(D(), pred())):
    if i % 100 == 0:
        print i
    try:
        res += np.log(pred * data + (1 - pred) * (1 - data))
    except NameError:
        res = np.log(pred * data + (1 - pred) * (1 - data))

np.savetxt(sys.argv[3], np.exp(res / (i + 1)))
    
    
