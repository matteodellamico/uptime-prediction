#!/usr/bin/env python

from __future__ import division

import sys
import numpy as np
import os.path
import random
import matplotlib
from matplotlib import pyplot as plt

matplotlib.rc('text', usetex=True)
matplotlib.rc('font', family='sans-serif', size=25)
matplotlib.rc('legend', fontsize=25)
matplotlib.rc('figure', figsize=(8, 6))

dirname = sys.argv[1]

D = np.loadtxt(os.path.join(dirname, 'D.gz')).astype(bool)
D = D.reshape((len(D), 6, 168)).mean(1)
#C = np.loadtxt(os.path.join(dirname, 'C.gz')).astype(bool)
#C = C.reshape((len(C), 6, 168)).mean(1)
pred = np.loadtxt(os.path.join(dirname, 'predictions_D.gz'))[:, :168]

xs = np.arange(0, 7, 1 / 24)

while True:
    node = random.randrange(len(D))

    print "Node #{}".format(node)

    plt.plot(xs, D[node], 'k-', label="Frequency online")
    plt.plot(xs, pred[node], 'b:', lw=4, label="Predictions")
#    plt.xlabel("Day of week")
#    plt.legend(loc=0)
    plt.ylim(-0.05, 1.05)
    plt.grid()
    plt.show()
