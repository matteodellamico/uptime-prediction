import cPickle as pickle

import datetime
import gzip
import numpy as np
import sys
import tempfile
import os.path

traceid, interval = sys.argv[1:]

with gzip.open('{}.pickle.gz'.format(traceid)) as f:
    trace = pickle.load(f)

start = datetime.datetime.fromtimestamp(trace.start)
stop = datetime.datetime.fromtimestamp(trace.stop)
fmt = '%Y-%m-%d %H:%M:%S'
print start.strftime(fmt), stop.strftime(fmt)
    
timeslots = xrange(int(trace.start), int(trace.stop), int(interval))

fname = 'avail_{}_{}.dat'.format(traceid, interval)
avail = np.memmap(fname, dtype='bool', mode='w+',
                  shape=(len(trace), len(timeslots)))

for i, im in enumerate(trace.itervalues()):
    if i % 100 == 0:
        print i
    avail[i] = [im[ts] for ts in timeslots]

