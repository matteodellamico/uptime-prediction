## OBSOLETE

from __future__ import division

import glob
import os.path
import sys
import numpy as np

directory = sys.argv[1]

pairs = [('training', 'test'), ('test', 'validation')]

for before, after in pairs:
    filename = os.path.join(directory, '{}.gz'.format(before))
    kept = np.loadtxt(filename).mean(1) > 1 / 6
    for fname in glob.iglob(os.path.join(directory, '*{}.gz'.format(after))):
        newname = fname[:-3] + '_filtered.gz'
        if os.path.exists(newname):
            continue
        np.savetxt(newname, np.loadtxt(fname)[kept])
        
