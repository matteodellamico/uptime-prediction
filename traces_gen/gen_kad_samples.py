from __future__ import division

import cPickle as pickle
import datetime
import gzip
import numpy as np
import random
import sys

traceid = 'kad'
interval, nsamples = map(int, sys.argv[1:])

with gzip.open('{}.pickle.gz'.format(traceid)) as f:
    trace = pickle.load(f)

start = datetime.datetime.fromtimestamp(trace.start)
stop = datetime.datetime.fromtimestamp(trace.stop)
fmt = '%Y-%m-%d %H:%M:%S'
print start.strftime(fmt), stop.strftime(fmt)
    
timeslots = xrange(int(trace.start), int(trace.stop), interval)

nodes = trace.keys()
random.shuffle(nodes)

per_sample = len(trace) / nsamples

cutpoints = [int(i * per_sample) for i in xrange(nsamples + 1)]

for i, (lo, hi) in enumerate(zip(cutpoints[:-1], cutpoints[1:])):

    print "Generating sample {}".format(i)
    
    sample_nodes = nodes[lo:hi]
    avail = np.zeros((len(sample_nodes), len(timeslots)), bool)
    for j, node in enumerate(sample_nodes):
        im = trace[node]
        avail[j] = [im[ts] for ts in timeslots]

    filename = 'avail_{}{}_{}.pickle.gz'.format(traceid, i, interval)
    print "Saving on {}...".format(filename)
    with gzip.open(filename, 'w') as f:
        pickle.dump(avail, f, pickle.HIGHEST_PROTOCOL)
    print "\tdone!"

    del avail

