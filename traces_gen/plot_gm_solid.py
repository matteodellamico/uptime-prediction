from __future__ import division

import numpy as np
import matplotlib
import gzip
from matplotlib import pyplot as plt

matplotlib.rc('text', usetex=True)
matplotlib.rc('font', family='serif', size=20)
matplotlib.rc('legend', fontsize=20)
matplotlib.rc('figure', figsize=(8, 4.5))
plt.figure().subplots_adjust(top=0.93, bottom=0.2)

datasets = [('imclean', 'IM', '-'),
            ('gw', 'GW', '-'),
            ('kad', 'Kad', '-')]

for ds, label, style in datasets:
    fn = '{}_4steps_24w/gm.gz'.format(ds)
    data = np.loadtxt(fn)
    if ds == 'gw':
        W = 24
    else:
        W = 24 * 7
    values = [data[i: i + W].mean() for i in range(24 * 7 * 6 - W)]
    plt.plot(np.arange(W, 24 * 7 * 6) / 24, values, style, label=label, lw=4)
    
plt.xlabel("Lookahead (days)")
plt.ylabel("GM (higher is better)")
plt.grid()
plt.legend(loc=0)
plt.show()
    
    
