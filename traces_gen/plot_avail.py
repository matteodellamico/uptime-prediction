from __future__ import division

import cPickle as pickle
import numpy as np
import matplotlib
import gzip
import sys
from matplotlib import pyplot as plt
from scipy.cluster import vq

matplotlib.rc('text', usetex=True)
matplotlib.rc('font', family='serif', size=20)
matplotlib.rc('legend', fontsize=20)
matplotlib.rc('figure', figsize=(8, 4))
plt.figure().subplots_adjust(bottom=0.17, top=0.95, right=0.96)

ds = sys.argv[1]
n = int(sys.argv[2])
            
fn = 'avail_{}_3600'.format(ds)
try:
    M = pickle.load(gzip.open(fn + '.pickle.gz'))
except IOError:
    M = np.memmap('/tmp/avail_kad_3600.dat',
                  dtype=bool, shape=(400375,4296))
centroids = pickle.load(gzip.open(
        'clusters_{}_3600_{}.pickle.gz'.format(ds, n)))

clusters = [[] for _ in xrange(n)]
code, _ = vq.vq(matrix, centroids)
for node, cluster in enumerate(code):
    clusters[cluster].add(node)

xs = np.arange(0, 18 * 7, 1 / 24)
for c in clusters:
    t = np.zeros(24 * 7 * 18, int)
    for node in c:
        t += line[:24 * 7 * 18]
    ys = t / len(c)
    plt.plot(xs, ys)
    print len(xs), M.shape
plt.plot(xs, ys)
plt.grid()
plt.xlabel("Day")
plt.ylabel("Fraction of nodes online")
plt.show()
