import cPickle as pickle
import datetime
import gzip
import numpy as np
import random
import sys

traceid, interval = sys.argv[1:]

with gzip.open('{}.pickle.gz'.format(traceid)) as f:
    trace = pickle.load(f)

start = datetime.datetime.fromtimestamp(trace.start)
stop = datetime.datetime.fromtimestamp(trace.stop)
fmt = '%Y-%m-%d %H:%M:%S'
print start.strftime(fmt), stop.strftime(fmt)
    
timeslots = xrange(int(trace.start), int(trace.stop), int(interval))

avail = np.zeros((len(trace), len(timeslots)), bool)

for i, im in enumerate(trace.itervalues()):
    avail[i] = [im[ts] for ts in timeslots]

with gzip.open('avail_{}_{}.pickle.gz'.format(traceid, interval),
               'w') as f:
    pickle.dump(avail, f, pickle.HIGHEST_PROTOCOL)
