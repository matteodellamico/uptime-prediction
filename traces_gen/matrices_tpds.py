from __future__ import division

import cPickle as pickle
import datetime
import gzip
import os
import os.path
import sys

traceid, interval = sys.argv[1:]
with gzip.open('{}.pickle.gz'.format(traceid)) as f:
    trace = pickle.load(f)

trace = trace.filter(1/6)

print len(trace), 'users'

start = datetime.datetime.fromtimestamp(trace.start)
stop = datetime.datetime.fromtimestamp(trace.stop)
fmt = '%Y-%m-%d %H:%M:%S'
print start.strftime(fmt), stop.strftime(fmt)

timeslots = xrange(int(trace.start), int(trace.stop), int(interval))
print len(timeslots), 'timeslots'

dirname = os.path.join('/tmp', traceid)
try:
    os.mkdir(dirname)
except OSError:
    pass

with gzip.open(os.path.join(dirname, 'avail.gz'), 'w') as f:
    for i, ts in enumerate(timeslots):
        if i % 1000 == 0:
            print i
        print >> f, ' '.join(str(tr[ts]) for tr in trace.itervalues())
