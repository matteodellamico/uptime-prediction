from __future__ import division

import cPickle as pickle
import functools
import sys
import gzip
import os, os.path
import numpy as np

from tempfile import mkdtemp

HOUR = 1
DAY = 24
WEEK = 24 * 7

traceid = sys.argv[1]

fname = 'avail_{}_3600.pickle.gz'.format(traceid)
dirname = traceid

with gzip.open(fname) as f:
    M = pickle.load(f)

nusers, nts = M.shape

nweeks = nts // WEEK
onethird = nweeks // 3 * WEEK
twothirds = onethird * 2

training, test, validation = (M[:, :onethird],
                              M[:, onethird:twothirds],
                              M[:, twothirds:])

validation_keepers = (test.mean(1) > 1 / 6).nonzero()
#test = test[(training.mean(1) > 1 / 6).nonzero()]
validation = validation[validation_keepers]

#os.mkdir(dirname)

def create_txtfile(matrix, filename):
    print "Saving {}...".format(filename),
    assert 0 <= matrix.min() <= matrix.max() <= 1
    np.savetxt(os.path.join(dirname, filename), matrix)
    print " done!"

#create_txtfile(training, 'training.gz')
#create_txtfile(test, 'test_filter2.gz')
create_txtfile(validation, 'validation_filter2.gz')

# daily_matrix = np.zeros((nusers, DAY), int)
# for j in range(DAY):
#     t = training[j::DAY]
#     daily_matrix[:, j] = t

# weekly_matrix = np.zeros((nusers, WEEK), int)
# for j in range(WEEK):
#     t = training[j::WEEK]
#     weekly_matrix[:, j] = t

# def flat(a, cols, o=None):
#     if o is None:
#         o = np.ones_like(a)
#     assert a.shape == o.shape
#     rows, _ = a.shape
#     pred = np.zeros((rows, cols), float)
#     for i in xrange(rows):
#         pred[i, :] = (a[i, :].sum() + 1) / (o[i, :].sum() + 2)
#     return pred

def periodic(a, cols, period, o=None):
    if o is None:
        o = np.ones_like(a)
    assert a.shape == o.shape
    rows, _ = a.shape
    pred = np.zeros((rows, cols), float)
    for i in xrange(rows):
        for j in range(period):
            sample_a = a[i, j::period]
            sample_o = o[i, j::period]
            pred[i, j::period] = (sample_a.sum() + 1) / (sample_o.sum() + 2)
    return pred

flat = functools.partial(periodic, period=HOUR)
daily = functools.partial(periodic, period=DAY)
weekly = functools.partial(periodic, period=WEEK)

predictors = [#('flat', flat),
              ('daily', daily),
              ('weekly', weekly)]

steps = [#('test', training, test.shape[1]),
         ('validation', test, validation.shape[1]),]

for pred_name, pred_f in predictors:
    for step_name, a, step_cols in steps:
        rows, cols = a.shape
        global_a = a.sum(0).reshape((1, cols))
        global_o = np.zeros((1, cols))
        global_o[:] = rows
        p_line = pred_f(global_a, step_cols, o=global_o)
        prediction = np.ones((rows, 1)) * p_line
        create_txtfile(prediction,
                       'global_{}_{}_filter2.gz'.format(pred_name, step_name))
        
# for pred_name, pred_f in predictors:
#     for step_name, a, step_cols in steps:
#         create_txtfile(pred_f(a, step_cols),
#                        'individual_{}_{}.gz'.format(pred_name, step_name))

