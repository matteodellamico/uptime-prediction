from __future__ import division

import cPickle as pickle
import functools
import sys
import gzip
import os, os.path
import numpy as np

HOUR = 1
DAY = 24
WEEK = 24 * 7

traceid = sys.argv[1]
weeks_per_step = 6

filtered = '--filter' in sys.argv

fname = 'avail_{}_3600.pickle.gz'.format(traceid)
dirname = traceid + '_conn'

if filtered:
    dirname += '_filtered'

with gzip.open(fname) as f:
    M = pickle.load(f)

M[:, 1:] &= M[:, :-1]
    
nusers, nts = M.shape

onefourth = weeks_per_step * WEEK

M = M[:, :onefourth * 4]

A = M[:, :onefourth]
B = M[:, onefourth:2 * onefourth]

if filtered:
    A_keepers = (A.mean(1) > 1 / 6).nonzero()
    A, B = A[A_keepers], B[A_keepers]

C = M[:, 2 * onefourth:3 * onefourth]
D = M[:, 3 * onefourth:]

if filtered:
    C_keepers = (C.mean(1) > 1 / 6).nonzero()
    C, D = C[C_keepers], D[C_keepers]

os.mkdir(dirname)

def create_txtfile(matrix, filename):
    print "Saving {}...".format(filename),
    assert 0 <= matrix.min() <= matrix.max() <= 1
    np.savetxt(os.path.join(dirname, filename), matrix)
    print " done!"

create_txtfile(A, 'A.gz')
create_txtfile(B, 'B.gz')
create_txtfile(C, 'C.gz')
create_txtfile(D, 'D.gz')

def periodic(a, period, o=None):
    if o is None:
        o = np.ones_like(a)
    assert a.shape == o.shape
    rows, _ = a.shape
    pred = np.zeros((rows, WEEK), float)
    for i in xrange(rows):
        for j in range(period):
            sample_a = a[i, j::period]
            sample_o = o[i, j::period]
            pred[i, j::period] = (sample_a.sum() + 1) / (sample_o.sum() + 2)
    return pred

flat = functools.partial(periodic, period=HOUR)
daily = functools.partial(periodic, period=DAY)
weekly = functools.partial(periodic, period=WEEK)

predictors = [('flat', flat),
              ('daily', daily),
              ('weekly', weekly)]

steps = [('B', A),
         ('D', C)]

for pred_name, pred_f in predictors:
    if pred_name == 'flat':
        continue
    for step_name, a in steps:
        rows, cols = a.shape
        global_a = a.sum(0).reshape((1, cols))
        global_o = np.zeros((1, cols))
        global_o[:] = rows
        p_line = pred_f(global_a, o=global_o)
        prediction = np.ones((rows, 1)) * p_line
        create_txtfile(np.tile(prediction, weeks_per_step),
                       'global_{}_{}.gz'.format(pred_name, step_name))
        
for pred_name, pred_f in predictors:
    for step_name, a, in steps:
        create_txtfile(np.tile(pred_f(a), weeks_per_step),
                       'individual_{}_{}.gz'.format(pred_name, step_name))

