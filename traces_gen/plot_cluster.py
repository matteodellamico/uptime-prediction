from __future__ import division

import cPickle as pickle
import numpy as np
import matplotlib
import gzip
import sys
from itertools import cycle
from matplotlib import pyplot as plt
from scipy.cluster import vq

matplotlib.rc('text', usetex=True)
matplotlib.rc('font', family='serif', size=20)
matplotlib.rc('legend', fontsize=20)
matplotlib.rc('figure', figsize=(8, 4))
plt.figure().subplots_adjust(bottom=0.17, top=0.95, right=0.96)

if len(sys.argv) >= 6:
    line_styles = sys.argv[5].split()
else:
    line_styles = "- -- -. :".split()
def cycle_styles(marker = ''):
    return cycle(s + marker for s in line_styles)
styles = cycle_styles()

ds = sys.argv[1]
n = int(sys.argv[2])
begin, end = map(float, sys.argv[3:5])

            
fn = 'avail_{}_3600'.format(ds)
try:
    M = pickle.load(gzip.open(fn + '.pickle.gz'))
except IOError:
    M = np.memmap('/tmp/avail_kad_3600.dat',
                  dtype=bool, shape=(400375,4296))

sub_m = M[:, int(begin * 24):int(end * 24)].astype(int)
while True:
    try:
        centroid, label = vq.kmeans2(sub_m, n, missing='raise')
    except vq.ClusterError:
        pass
    else:
        break
    
clusters = [[] for _ in xrange(n)]
for node, cluster in enumerate(label):
    clusters[cluster].append(node)

xs = np.arange(begin, end, 1 / 24)
for c in clusters:
    t = np.zeros(sub_m.shape[1], int)
    for node in c:
        t += sub_m[node, :]
    ys = t / len(c)
    plt.plot(xs, ys, next(styles),
             linewidth=5,
             label="{}".format(len(c)))
plt.grid()
plt.xlabel("Day")
plt.ylabel("Fraction of nodes online")
plt.ylim(0, 1.2)
plt.xlim(begin, end)
plt.legend(loc=0, ncol=4, fontsize=15)
plt.show()
