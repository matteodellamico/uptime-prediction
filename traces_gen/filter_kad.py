from __future__ import division

import functools
import numpy as np
import os.path

HOUR = 1
DAY = 24
WEEK = DAY * 7

nusers = 400375
nts = 4296

avail = np.memmap('/tmp/avail_kad_3600.dat', dtype=bool, mode='r',
                  shape=(nusers, nts))

weeks_per_step = 6

A_lim, B_lim, C_lim, D_lim = (i * 24 * 7 * weeks_per_step
                              for i in range(1, 5))

in_A, in_C = [], []

for i in xrange(nusers):
    if avail[i, :A_lim].mean() >  1 / 6:
        in_A.append(i)
    if avail[i, B_lim:C_lim].mean() > 1 / 6:
        in_C.append(i)

A = avail[in_A, :A_lim]
B = avail[in_A, A_lim:B_lim]
C = avail[in_C, B_lim:C_lim]
D = avail[in_C, C_lim:D_lim]

dirname = 'kad_4steps_24w_filtered'
os.mkdir(dirname)

def create_txtfile(matrix, filename):
    print "Saving {}...".format(filename),
    assert 0 <= matrix.min() <= matrix.max() <= 1
    np.savetxt(os.path.join(dirname, filename), matrix)
    print " done!"

create_txtfile(A, 'A.gz')
create_txtfile(B, 'B.gz')
create_txtfile(C, 'C.gz')
create_txtfile(D, 'D.gz')

def periodic(a, period, o=None):
    if o is None:
        o = np.ones_like(a)
    assert a.shape == o.shape
    rows, _ = a.shape
    pred = np.zeros((rows, WEEK), float)
    for i in xrange(rows):
        for j in range(period):
            sample_a = a[i, j::period]
            sample_o = o[i, j::period]
            pred[i, j::period] = (sample_a.sum() + 1) / (sample_o.sum() + 2)
    return pred

flat = functools.partial(periodic, period=HOUR)
daily = functools.partial(periodic, period=DAY)
weekly = functools.partial(periodic, period=WEEK)

predictors = [('flat', flat),
              ('daily', daily),
              ('weekly', weekly)]

steps = [('B', A),
         ('D', C)]

for pred_name, pred_f in predictors:
    for step_name, a in steps:
        rows, cols = a.shape
        global_a = a.sum(0).reshape((1, cols))
        global_o = np.zeros((1, cols))
        global_o[:] = rows
        p_line = pred_f(global_a, o=global_o)
        prediction = np.ones((rows, 1)) * p_line
        create_txtfile(np.tile(prediction, weeks_per_step),
                       'global_{}_{}.gz'.format(pred_name, step_name))
        
for pred_name, pred_f in predictors:
    for step_name, a, in steps:
        create_txtfile(np.tile(pred_f(a), weeks_per_step),
                       'individual_{}_{}.gz'.format(pred_name, step_name))
