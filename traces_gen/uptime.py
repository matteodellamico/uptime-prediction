from __future__ import division

import interval_mapping
from numpy import array

MINUTE = 60
HOUR = 60 * MINUTE
DAY = 24 * HOUR
WEEK = 7 * DAY
MONTH = 30 * DAY
YEAR = 365 * DAY

#def pylabtime(t):
#    # 978303600 = 0001-01-01 00:00:00
#    return (t - 978303600) / (24 * 60 * 60)

class Trace(dict):

    @property
    def start(self):
        try:
            return self._start
        except AttributeError:
            self._start = start = min(m._bounds[0] for m in self.itervalues()
                                      if len(m._bounds))
            return start

    @property
    def stop(self):
        try:
            return self._stop
        except AttributeError:
            self._stop = stop = max(m._bounds[-1] for m in self.itervalues()
                                    if len(m._bounds))
            return stop

    def uptime(self, node, start=None, stop=None):
        if start is None:
            start = self.start
        if stop is None:
            stop = self.stop
        return (sum(min(i.stop, stop) - max(i.start, start)
                    for i, state in self[node].iteritems()
                    if state and i.stop > start and i.start < stop)
                / (stop - start))

    def nup(self, node, start=None, stop=None):
        if start is None:
            start = self.start
        if stop is None:
            stop = self.stop
        return (sum(min(i.stop, stop) - max(i.start, start)
                    for i, state in self[node].iteritems()
                    if state and i.stop > start and i.start < stop),
                stop - start)
    
    def filter(self, threshold=0.17, start=None, stop=None):
        res = Trace()
        for node in self:
            if self.uptime(node, start, stop) >= threshold:
                res[node] = self[node]
        return res

    def subtrace(self, start, stop):
        res = Trace((k, v.submapping(start, stop)) for k, v in self.iteritems())
        for m in res.itervalues():
            m[:start] = 0
            m[stop:] = 0
        return res

    def mortality(self, threshold=MONTH):
        last_alive = self.stop - threshold
        if last_alive < self.start:
            raise ValueError
        return (sum(len(t._bounds) == 0 or t._bounds[-1] < last_alive
                   for t in self.itervalues())
                / len(self) / (last_alive - self.start))

def aggregate(traces):
    return interval_mapping.apply(lambda *xs: sum(xs), *traces.values())
