#!/usr/bin/env python

"""Simulation of node placement for a storage system given
availability prediction."""

from __future__ import division

import itertools
import numpy as np
import random
import sys
    
def data_unav(u_matrix, stored):
    """Data unavailability matrix -- lines are owners, columns timeslots."""

    holders = [[] for _ in xrange(len(u_matrix))]
    for i, owners in enumerate(stored):
        for j in owners:
            holders[j].append(i)

    return np.array([u_matrix[h_set].prod(0) for h_set in holders])

def data_unav_2(u_matrix, stored):
    """Debugging version..."""

    holders = [[] for _ in xrange(len(u_matrix))]
    for i, owners in enumerate(stored):
        for j in owners:
            holders[j].append(i)

    result = np.empty_like(u_matrix)
    for i, h_set in enumerate(holders):
        unav = np.ones(u_matrix.shape[1])
        for h in h_set:
            unav *= u_matrix[h]
        result[i] = unav
    return result

def data_unav_3(u_matrix, stored):
    """More debugging."""

    d_u = np.ones_like(u_matrix)
    for unav, items in itertools.izip(u_matrix, stored):
        d_u[items, :] *= unav
    return d_u

def storage_sim(pred, units, eligible=None, verbose=True):
    """Run the simulation.

    pred -- matrix of predictions (stripped away of periodic parts)
    units -- storage units per node
    eligible -- eligible data holders per node

    Yields, at each iteration:
     - data placement
     - matrix of predicted unavailability
     - number of swapped pieces of data
    """

    nnodes = len(pred)

    if eligible is None:
        eligible = [range(nnodes)] * nnodes
    
    # one minus -- log of probability of unavailability
    pred = 1 - pred

    # let's start with random data storage
    stored = np.empty((nnodes, units), int)
    free = []
    for i, neighbors in enumerate(eligible):
        #perm = np.random.permutation(neighbors)
        perm = np.copy(neighbors)
        stored[i] = perm[:units]
        free.append(perm[units:])

    d_u = data_unav(pred, stored)
    swapped = None
    print "Starting predicted unavailability", d_u.mean()
    for iter_no in itertools.count():
        
        yield stored, d_u, swapped

        if swapped == 0:
            raise StopIteration
        
        swapped = 0
    
        for i, (u, s, f) in enumerate(itertools.izip(pred, stored, free)):

            # first, let's find a node to remove
            
            # current data unavailabilities of owners
            cur_unav = d_u[s]
            
            # new data unavailabilities if a unit is removed
            new_unav = cur_unav / u

            # differences in availability
            rem_deltas = (new_unav - cur_unav).sum(1)
           
            # index of node that would create less problems if removed
            rem_idx = np.argmin(rem_deltas)
            rem_delta = rem_deltas[rem_idx]

#            assert rem_delta >= 0 # unavailability increase

            # find the most beneficial node to include

            # unavailabilities of free owners
            cur_free_u = d_u[f]
            
            # new unavailabilities if a unit is included
            new_free_u = cur_free_u * u

            # deltas
            add_deltas = (new_free_u - cur_free_u).sum(1)

            # node that would solve most if added
            add_idx = np.argmin(add_deltas)
            add_delta = add_deltas[add_idx]

#            assert add_delta <= 0 # unavailability decreases
            
            # if there's enhancement, swap them
            if add_delta + rem_delta < -0.001: # not 0, for numerical stab
                swapped += 1
                included, removed = f[add_idx], s[rem_idx]
                stored[i][rem_idx], free[i][add_idx] = included, removed
                d_u[removed] = new_unav[rem_idx]
                d_u[included] = new_free_u[add_idx]
                
        if verbose:
            print iter_no
            print "Predicted unavailability", d_u.mean()
        d_u = data_unav(pred, stored)
        if verbose:
            print "Correct predicted unavailability", d_u.mean()
#            print "...debug:", data_unav_3(pred, stored).mean()
            print "Swapped:", swapped

    swapped = 0

def r_and_a(a, units, eligible=None):
    """Adapted from Kermarrec et al., Availability-based methods..."""

    nnodes = len(a)
    
    if eligible is None:
        eligible = [range(nnodes) for _ in xrange(nnodes)]
    else:
        eligible = [list(l) for l in eligible]

    free_space = np.repeat(units, nnodes)
    active = set(xrange(nnodes))

    stored = [[] for _ in xrange(nnodes)]
    
    while active:
        print "..."
        for node in list(active):
            
            # filter all neighbors with 0 free space
            neighbors = [n for n in eligible[node] if free_space[n] > 0]
            eligible[node] = neighbors
            
            # R

            try:
                r_node = random.choice(neighbors)
            except IndexError:
                active.remove(node)
                continue
            neighbors.remove(r_node)
            stored[r_node].append(node)
            free_space[r_node] -= 1

            # A
            scores = (a[neighbors] ^ a[r_node]).sum(1)
            assert len(scores) == len(neighbors)
            try:
                a_node_idx = scores.argmax()
            except ValueError:
                active.remove(node)
                continue
            a_node = neighbors[a_node_idx]
            neighbors.remove(a_node)
            stored[a_node].append(node)
            free_space[a_node] -= 1

    return stored

def ra2(a, units, eligible=None):
    """Adapted from Kermarrec et al., Availability-based methods..."""

    nnodes = len(a)
    
    if eligible is None:
        eligible = [range(nnodes) for _ in xrange(nnodes)]
    else:
        eligible = [list(l) for l in eligible]

    free_space = np.repeat(units, nnodes)
    active = set(xrange(nnodes))

    stored = [[] for _ in xrange(nnodes)]
    
    last_r = {}
    while active:
        
        print "R..."
        for node in list(active):
            
            # filter all neighbors with 0 free space
            neighbors = [n for n in eligible[node] if free_space[n] > 0]
            eligible[node] = neighbors
            
            # R

            try:
                r_node = random.choice(neighbors)
            except IndexError:
                active.remove(node)
                continue
            neighbors.remove(r_node)
            stored[r_node].append(node)
            free_space[r_node] -= 1
            last_r[node] = r_node

        for node in list(active):

            # A
            
            scores = (a[neighbors] ^ a[last_r[node]]).sum(1)
            assert len(scores) == len(neighbors)
            try:
                a_node_idx = scores.argmax()
            except ValueError:
                active.remove(node)
                continue
            a_node = neighbors[a_node_idx]
            neighbors.remove(a_node)
            stored[a_node].append(node)
            free_space[a_node] -= 1

    return stored

def main():
    """Parameters in sys.argv:

    pred -- file containing predictions
    avail_train -- file with availability in training set
    avail_test -- file containing availability in test set
    r -- redundancy
    period -- periodicity of predictions (detected from filename if absent)
    """

    import networkx as nx
    
    pred, avail_train, avail, redundancy = sys.argv[1:]

    pred = np.loadtxt(pred)[:, :24 * 7]
    assert ((0 < pred) & (pred < 1)).all()

    avail_train = np.loadtxt(avail_train)
    assert ((avail_train == 0) | (avail_train == 1)).all()
    avail_train = avail_train.astype(bool)
    
    avail = np.loadtxt(avail)
    assert ((avail == 0) | (avail == 1)).all()
    avail = avail.astype(bool)

    redundancy = int(redundancy)
    
    if len(pred) != len(avail):
        raise ValueError("Matrices have different # of nodes")

    print len(avail), "nodes"

    graph = nx.generators.watts_strogatz_graph(len(avail), 20, 0.5)
    
    eligible = graph.adjacency_list()

    sim = storage_sim(pred, redundancy, eligible)

    stored, _, _ = next(sim)

    starting_mean = data_unav(1 - avail, stored).mean()
    print "Starting unavailability:", starting_mean

    for stored, _, _ in sim:
        pass
    
    end_mean = data_unav(1 - avail, stored).mean()
    print "End unavailability: ", end_mean

    ra_placement = r_and_a(avail_train, redundancy, eligible)
    print "R&A unavailability: ", data_unav(1 - avail, ra_placement).mean()

if __name__ == '__main__':
    main()
