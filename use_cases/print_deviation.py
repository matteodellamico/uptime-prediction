#!/usr/bin/env python

import numpy as np
import os.path
import sys

dirname = sys.argv[1]

errsi = np.empty(100)
errsf = np.empty(100)
for i in range(100):
    fname = os.path.join(dirname, "predunav_init_{0}.gz".format(i))
    pi = np.loadtxt(fname).mean()
    fname = os.path.join(dirname, "unav_init_{0}.gz".format(i))
    ri = np.loadtxt(fname).mean()
    errsi[i] = pi - ri
    fname = os.path.join(dirname, "predunav_final_{0}.gz".format(i))
    pf = np.loadtxt(fname).mean()
    fname = os.path.join(dirname, "unav_final_{0}.gz".format(i))
    rf = np.loadtxt(fname).mean()
    errsf[i] = pf - rf

print errsi
print "Init:", np.abs(errsi).mean()
print errsf
print "Final:", np.abs(errsf).mean()
