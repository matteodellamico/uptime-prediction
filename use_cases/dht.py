#!/usr/bin/env python

"""Simulation of node placement on a DHT given availability prediction."""

from __future__ import division

import itertools
import numpy as np
import random
import sys
    
def followers_predecessors(nodes, redundancy):
    """followers_predecessors(n, r) -> followers, predecessors

    followers[i, j] = j-th follower of position i
    predecessors[i, j] = j-th predecessor of position i
    """

    lines = np.arange(nodes)
    lines.resize(nodes, 1)
    cols = np.arange(redundancy)
    return (lines + cols) % nodes, (lines - cols) % nodes

def data_unav(dht, node_unav, followers):
    """data_a(dht, node_unav, followers) -> a

    a[i, j] is the probability that object i is available at timeslot j
    """

    # buggy -- should understand what's wrong with this
    # return np.array([node_unav[holders].prod(0)
    #                  for holders in dht[followers[dht]]])
    return np.array([node_unav[dht[followers[i]]].prod(0)
                     for i in range(len(dht))])

def dhtsim(pred, redundancy, verbose=True, ):
    """Run the simulation.

    pred -- matrix of predictions (stripped away of periodic parts)
    redundancy -- how many copies are stored

    Yields, at each iteration:
     - node placement
     - matrix of predicted availability
     - number of swapped nodes
    """
    
    # one minus -- probability of unavailability
    pred = 1 - pred

    nnodes = len(pred)

    # Mapping of position to node-id
    dht = np.random.permutation(nnodes)

    # Mapping of node-id to position
    pos = np.empty(nnodes, int)
    for i, node in enumerate(dht):
        pos[node] = i

    followers, predecessors = followers_predecessors(nnodes, redundancy)

    p_unavail = data_unav(dht, pred, followers)
    
    def try_swapping(pos1, pos2):
        """Try swapping nodes at position pos1 and pos2.

        Return True if they were finally swapped.
        """

        node1, node2 = dht[pos1], dht[pos2]
        predec1, predec2 = predecessors[pos1], predecessors[pos2]
        now1, now2 = p_unavail[predec1], p_unavail[predec2]
        pred1, pred2 = pred[[node1, node2]]
        new1 = now1 / pred1 * pred2
        new2 = now2 / pred2 * pred1
        if new1.sum() + new2.sum() < now1.sum() + now2.sum():
            dht[[pos1, pos2]] = node2, node1
            pos[[node1, node2]] = pos2, pos1
            p_unavail[predec1] = new1
            p_unavail[predec2] = new2
            return True
        else:
            return False
    
    swapped = 0
    for iter_no in itertools.count():
        yield dht, p_unavail, swapped
        swapped = 0
        for i in range(nnodes):
            j = random.randrange(nnodes)
            swapped += try_swapping(i, j)
        if iter_no % 100 == 0:
            if verbose:
                print iter_no
                print "Predicted unavailability", p_unavail.mean()
            p_unavail = data_unav(dht, pred, followers)
            if verbose:
                print "Correct predicted unavailability", p_unavail.mean()
                print "Swapped:", swapped

    swapped = 0

def main():
    """Parameters in sys.argv:

    pred -- file containing predictions
    avail -- file containing availability
    r -- redundancy
    period -- periodicity of predictions (detected from filename if absent)
    """

    try:
        pred, avail, redundancy, period = sys.argv[1:]
        period = int(period)
    except ValueError:
        pred, avail, redundancy = sys.argv[1:]
        if 'flat' in pred:
            period = 1 # a timeslot is one hour
        elif 'daily' in pred:
            period = 24
        elif 'weekly' in pred:
            period = 24 * 7
        else:
            period = None

    pred = np.loadtxt(pred)
    assert ((0 < pred) & (pred < 1)).all()

    avail = np.loadtxt(avail)
    assert ((avail == 0) | (avail == 1)).all()
    avail = avail.astype(bool)

    redundancy = int(redundancy)
    
    if pred.shape != avail.shape:
        raise ValueError("Matrices have inconsistent shapes")

    if period is not None:
        pred = pred[:, :period] # let's exploit periodicity in predictions

    print len(avail), "nodes"

    followers, _ = followers_predecessors(len(avail), redundancy)
    
    sim = dhtsim(pred, redundancy)

    dht, _, _ = next(sim)

    starting_mean = data_unav(dht, 1 - avail, followers).mean()
    print "Starting unavailability:", starting_mean
    
    for _ in range(1000):
        dht, _ , _ = next(sim)

    end_mean = data_unav(dht, 1 - avail, followers).mean()
    print "End unavailability: ", end_mean

if __name__ == '__main__':
    main()
