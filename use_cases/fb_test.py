import itertools
import sys
import os.path
import numpy as np
import cPickle as pickle
import gzip

dirname = sys.argv[1]

predictions = np.loadtxt(os.path.join(dirname, 'individual_daily_D.gz')).T
C_avails = np.loadtxt(os.path.join(dirname, 'C.gz')).astype(bool).mean(1)
D = np.loadtxt(os.path.join(dirname, 'D.gz')).astype(bool).T


result_dir = os.path.join(dirname, 'fbsim')
try:
    os.mkdir(result_dir)
except OSError:
    pass

bya = np.zeros_like(D)
byp = np.zeros_like(D)
available_before = np.zeros(D.shape[0], bool)
for i, (pred, avail) in enumerate(itertools.izip(predictions, D)):
    interesting = (~available_before).nonzero()
#    print interesting
    C_order = (-C_avails[interesting]).argsort()
    aa = avail[interesting][C_order]
    bya[i, :len(aa)] = aa
    pp = avail[interesting][(-pred[interesting]).argsort()]
    byp[i, :len(pp)] = pp
    available_before = avail

np.savetxt(os.path.join(result_dir, 'by_avail_test.gz'), np.array(bya))
np.savetxt(os.path.join(result_dir, 'by_pred_test.gz'), np.array(byp))
