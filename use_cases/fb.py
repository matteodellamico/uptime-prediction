import itertools
import sys
import os.path
import numpy as np

dirname = sys.argv[1]

predictions = np.loadtxt(os.path.join(dirname, 'predictions_D.gz')).T
C_avails = np.loadtxt(os.path.join(dirname, 'C.gz')).astype(bool).mean(1)
D = np.loadtxt(os.path.join(dirname, 'D.gz')).astype(bool).T

result_dir = os.path.join(dirname, 'fbsim')
try:
    os.mkdir(result_dir)
except OSError:
    pass


C_order = (-C_avails).argsort()
bya, byp = [], []
for pred, avail in itertools.izip(predictions, D):
    bya.append(avail[C_order])
    byp.append(avail[(-pred).argsort()])

np.savetxt(os.path.join(result_dir, 'by_avail.gz'), np.array(bya))
np.savetxt(os.path.join(result_dir, 'by_pred.gz'), np.array(byp))
