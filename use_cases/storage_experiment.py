#!/usr/bin/env python

import storage
import sys
import os.path
import numpy as np
import random
import networkx as nx

dirname = sys.argv[1]
redundancy = int(sys.argv[2])
repetitions = int(sys.argv[3])

predictions = np.loadtxt(os.path.join(dirname, 'predictions_D.gz'))[:, :24 * 7]
D = np.loadtxt(os.path.join(dirname, 'D.gz'))

result_dir = os.path.join(dirname, 'storage_sim')
os.mkdir(result_dir)

for i in xrange(repetitions):
    print "Experiment", i

    graph = nx.generators.watts_strogatz_graph(408, 20, 0.5)
    thenodes = random.sample(xrange(len(D)), 408)
    pred = predictions[thenodes]
    avail = D[thenodes]

    eligible = graph.adjacency_list()

    sim = storage.storage_sim(pred, redundancy, eligible)

    stored, _, _ = next(sim)

    np.savetxt(os.path.join(result_dir, 'unav_init_{0}.gz'.format(i)),
               storage.data_unav(1 - avail, stored))

    np.savetxt(os.path.join(result_dir, 'predunav_init_{0}.gz'.format(i)),
               storage.data_unav(1 - pred, stored))

    for stored, _, _ in sim:
        pass

    np.savetxt(os.path.join(result_dir, 'unav_final_{0}.gz'.format(i)),
               storage.data_unav(1 - avail, stored))

    np.savetxt(os.path.join(result_dir, 'predunav_final_{0}.gz'.format(i)),
               storage.data_unav(1 - pred, stored))
