import dht
import sys
import os.path
import numpy as np
import random

dirname = sys.argv[1]
redundancy = int(sys.argv[2])
repetitions = int(sys.argv[3])

predictions = np.loadtxt(os.path.join(dirname, 'predictions_D.gz'))[:, :24 * 7]
D = np.loadtxt(os.path.join(dirname, 'D.gz'))

result_dir = os.path.join(dirname, 'dhtsim')
os.mkdir(result_dir)

followers, _ = dht.followers_predecessors(408, redundancy)

for i in xrange(repetitions):
    print "Experiment", i

    thenodes = random.sample(xrange(len(D)), 408)

    pred = predictions[thenodes]
    avail = D[thenodes]

    sim = dht.dhtsim(pred, redundancy)

    thedht, _, _ = next(sim)

    np.savetxt(os.path.join(result_dir, 'unav_init_{0}.gz'.format(i)),
               dht.data_unav(thedht, 1 - avail, followers))

    np.savetxt(os.path.join(result_dir, 'predunav_init_{0}.gz'.format(i)),
               dht.data_unav(thedht, 1 - pred, followers))

    for _ in xrange(1000):
        thedht, _, _ = next(sim)

    np.savetxt(os.path.join(result_dir, 'unav_final_{0}.gz'.format(i)),
               dht.data_unav(thedht, 1 - avail, followers))

    np.savetxt(os.path.join(result_dir, 'predunav_final_{0}.gz'.format(i)),
               dht.data_unav(thedht, 1 - pred, followers))

