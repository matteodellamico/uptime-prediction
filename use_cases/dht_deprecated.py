from __future__ import division

import random
import numpy as np
import sys

DAY = 24
WEEK = 24 * 7

if len(sys.argv) == 4:    
    pred, avail, r, period = sys.argv[1:]
else:
    pred, avail, r = sys.argv[1:]
    if 'flat' in pred:
        period = 1
    elif 'daily' in pred:
        period = DAY
    elif 'weekly' in pred:
        period = WEEK
    else:
        period = None

# one minus -- probability of unavailability
pred = 1 - np.loadtxt(pred)
assert ((0 < pred) & (pred < 1)).all()

avail = 1 - np.loadtxt(avail)
assert ((avail == 0) | (avail == 1)).all()
avail = avail.astype(bool)

if pred.shape != avail.shape:
    raise ValueError("Matrices have inconsistent shapes")

if period is not None:
    pred = pred[:, :period] # let's exploit periodicity in predictions

print len(pred), "nodes before filtering"

# let's filter only nodes that are predicted to stay online more than
# 4 hours per day

filtered_nodes, = np.where(pred.mean(1) < (5 / 6))
pred = pred[filtered_nodes]
avail = avail[filtered_nodes]

print len(filtered_nodes), "nodes after filtering"

r = int(r)

nnodes, nts = avail.shape

# Mapping of position to node-id
dht = np.arange(nnodes)
random.shuffle(dht)

# Mapping of node-id to position
pos = np.zeros_like(dht)
for i, node in enumerate(dht):
    pos[node] = i

followers = np.array([[(i + j) % nnodes for j in range(r)]
                      for i in range(nnodes)])

predecessors = np.array([[(i - j) % nnodes for j in range(r)]
                         for i in range(nnodes)])

p_unavail = np.array([pred[dht[followers[i]]].prod(0) for i in xrange(nnodes)])
print "Predicted unavailability", p_unavail.mean()

assert p_unavail.shape == pred.shape

r_unavail = np.array([avail[dht[followers[i]]].prod(0) for i in xrange(nnodes)])
print "Real unavailability", r_unavail.mean()

for iterno in xrange(10000):
    swapped = 0
    for i in xrange(nnodes):
        j = random.randrange(nnodes)
        pos_i = pos[i]
        pos_j = pos[j]
        predec_pos_i = predecessors[pos_i]
        predec_pos_j = predecessors[pos_j]
        predec_nodes_i = dht[predec_pos_i]
        predec_nodes_j = dht[predec_pos_j]
        now_pred_i = p_unavail[predec_pos_i]
        now_pred_j = p_unavail[predec_pos_j]
        u_i = pred[i, :]
        u_j = pred[j, :]
        now = now_pred_i.sum() + now_pred_j.sum()
        new_pred_i = now_pred_i / u_i * u_j
        new_pred_j = now_pred_j / u_j * u_i
        new = new_pred_i.sum() + new_pred_j.sum()
        if new < now:
            swapped += 1
            # swap node i and j
            dht[pos[i]] = j
            dht[pos[j]] = i
            pos[i], pos[j] = pos[j], pos[i]
            p_unavail[predec_pos_i] = new_pred_i
            p_unavail[predec_pos_j] = new_pred_j
    if iterno % 100 == 0:
        print iterno
        print "Predicted unavailability", p_unavail.mean()
        p_unavail = np.array([pred[dht[followers[i]]].prod(0)
                              for i in xrange(nnodes)])
        print "Correct predicted unavailability", p_unavail.mean()
#    r_unavail = np.array([avail[dht[followers[i]]].prod(0)
#                          for i in xrange(nnodes)])
#    print "Real unavailability", r_unavail.mean()
        print "Swapped {} out of {}: ratio {}".format(swapped, nnodes,
                                                      swapped / nnodes)


r_unavail = np.array([avail[dht[followers[i]]].prod(0)
                      for i in xrange(nnodes)])
print "Real unavailability", r_unavail.mean()
