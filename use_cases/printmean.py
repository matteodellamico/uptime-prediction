#!/usr/bin/env python

import numpy as np
import sys

av = 1 - np.array([np.loadtxt(fname).mean() for fname in sys.argv[1:]])

print av
print av.mean()
print np.std(av)
