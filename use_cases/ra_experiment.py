#!/usr/bin/env python

import storage
import sys
import os.path
import numpy as np
import random
import networkx as nx

dirname = sys.argv[1]
redundancy = int(sys.argv[2])
repetitions = int(sys.argv[3])

C = np.loadtxt(os.path.join(dirname, 'C.gz')).astype(bool)
D = np.loadtxt(os.path.join(dirname, 'D.gz')).astype(bool)

result_dir = os.path.join(dirname, 'storage_sim')

nnodes = len(C)

for i in xrange(repetitions):
    print "Experiment", i

    graph = nx.generators.watts_strogatz_graph(408, 20, 0.5)
    thenodes = random.sample(xrange(len(D)), 408)
    a = C[thenodes]
    avail = D[thenodes]

    eligible = graph.adjacency_list()

    ra_placement = storage.r_and_a(a, redundancy, eligible)

    unav = storage.data_unav(1 - avail, ra_placement)

    print "Avail", 1 - unav.mean()
    
    np.savetxt(os.path.join(result_dir, 'ra_unav_{0}.gz'.format(i)), unav)
