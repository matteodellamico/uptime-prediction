#!/usr/bin/env python

import numpy as np
import sys

unav = np.array([np.loadtxt(fname).mean() for fname in sys.argv[1:-1]])

np.savetxt(sys.argv[-1], unav)

