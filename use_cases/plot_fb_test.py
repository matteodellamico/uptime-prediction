import sys
import os.path
import numpy as np
from matplotlib import pyplot as plt

dirnames = ['../traces_gen/imclean_conn']

labels = 'IM'.split()

for dirname, label in zip(dirnames, labels):
    fname = os.path.join(dirname, 'fbsim', 'by_pred_test.gz')
    byp = np.loadtxt(fname).astype(bool).mean(0)
    pp = byp.cumsum() / np.arange(1, len(byp) + 1)
    plt.semilogx(pp, label=label)
    fname = os.path.join(dirname, 'fbsim', 'by_avail_test.gz')
    byp = np.loadtxt(fname).astype(bool).mean(0)
    pp = byp.cumsum() / np.arange(1, len(byp) + 1)
    plt.semilogx(pp, label=label+'avail')
plt.grid()
plt.legend(loc=0)
#plt.xlim(xmin=30)
plt.xlabel("Nodes in push mode")
plt.ylabel("Requested ratio (number of nines)")
plt.show()
