import sys
import os.path
import numpy as np
from matplotlib import pyplot as plt
import matplotlib

matplotlib.rc('text', usetex=True)
matplotlib.rc('font', family='serif', size=20)
matplotlib.rc('legend', fontsize=20)
matplotlib.rc('figure', figsize=(8, 6))

dirnames = [#'../traces_gen/gw_4steps_24w',
#            '../traces_gen/kad1_4steps_24w',
            '../traces_gen/imclean_4steps_24w',
            '../traces_gen/kad0_4steps_24w',
            ]

labels = 'IM Kad0'.split()

styles = iter(['-', '--', ':', '-.'])

for dirname, label in zip(dirnames, labels):
    fname = os.path.join(dirname, 'fbsim', 'by_pred2.gz')
    byp = np.loadtxt(fname).astype(bool).mean(0)
    pp = byp.cumsum() / np.arange(1, len(byp) + 1)
    plt.plot(range(1, len(pp) + 1), pp, next(styles), label=label, lw=4)
    fname = os.path.join(dirname, 'fbsim', 'by_avail2.gz')
    byp = np.loadtxt(fname).astype(bool).mean(0)
    pp = byp.cumsum() / np.arange(1, len(byp) + 1)
    plt.plot(range(1, len(pp) + 1), pp, next(styles),
             label=label+' (baseline)', lw=4)
plt.grid()
plt.legend(loc=0)
plt.xlim(1, 400)
plt.xlabel("Users in push mode")
plt.ylabel("Hit rate")
plt.show()
