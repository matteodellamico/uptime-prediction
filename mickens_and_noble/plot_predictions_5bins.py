from __future__ import division

import cPickle as pickle
import gzip
import itertools

import numpy as np
import pylab

try:
    samples = np.loadtxt('samples')
    im = False
except IOError:
    samples = pickle.load(open('bins.pickle'))
    missing = list(set(xrange(1174)) - set(itertools.chain(*samples)))
    samples.append(missing)
    im = True

bins = {}
for i, nodes in enumerate(samples):
    for node in nodes:
        bins[node] = i

if im:
    assert sorted(bins) == range(1174)
else:
    assert len(bins) == 1000
    
results = [[0, 0, 0] for _ in xrange(11 if im else 10)]

def D():
    file = gzip.open('../D.gz')
    for i, line in enumerate(file):
        if i not in bins:
            continue
        yield map(bool, map(float, line.strip().split()[::6]))

def logreg():
    # try:
    #     file = gzip.open('../predictions_D.gz')
    # except IOError:
    #     while True:
    #         yield [False] * 168
    file = gzip.open('../predictions_D.gz')
    for i, line in enumerate(file):
        if i not in bins:
            continue
        r = map(float, line.strip().split()[::6])
        yield [p > 0.5 for p in r]

def tournament():
    file = open('tournament')
    for line in file:
        try:
            yield map(bool, map(int, line.strip().split()))
        except ValueError:
            #bug -- a print for debug remained here
            continue

def thebins():
    for k, v in sorted(bins.iteritems()):
        yield v
        
for (d, l, t, bin) in itertools.izip(D(), logreg(), tournament(), thebins()):
    
    assert len(d) == 24 * 7 * 6 // 6
    
    tot, log_score, tourn_score = results[bin]

    log_score += sum(v == p for v, p in zip(d, l))
    tourn_score += sum(v == p for v, p in zip(d, t))

    results[bin] = (tot + len(d), log_score, tourn_score)

print results

it = iter(results)
results = [((d1 + d2), (l1 + l2), (t1 + t2))
           for (d1, l1, t1), (d2, l2, t2) in zip(it, it)]

pylab.plot([l / d for d, l, _ in results], label='log_reg')
pylab.plot([t / d for d, _, t in results], label='tournament')
pylab.xlabel('Availability class')
pylab.ylabel('Accuracy')
#pylab.yrange(0.5, 1)
pylab.legend(loc=0)
pylab.show()
