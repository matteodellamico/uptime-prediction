import gzip
import itertools
import operator
import random
import sys

import numpy as np

import predictors

A = gzip.open('A.gz')
B = gzip.open('B.gz')
C = gzip.open('C.gz')

avails = np.loadtxt('D.gz').mean(1)

bins = [np.where((avails >= i * 0.1) & (avails < (i + 1) * 0.1))[0]
        for i in range(10)]

samples = np.array([random.sample(bin, 100) for bin in bins])

np.savetxt('mn/samples', samples)

sampled = set(samples.flatten())

def readvals(f):
    for l in f:
        yield map(int, map(float, l.strip().split()[::6]))

for i, lines in enumerate(itertools.izip(*map(readvals, [A, B, C]))):

    if i not in sampled:
        continue
    training = reduce(operator.add, lines)
    assert len(training) == 18 * 7 * 24 // 6

    hist_tournament = predictors.Tournament(
        predictors.Tournament(predictors.History(6),
                              predictors.History(24)),
        predictors.Tournament(predictors.History(48),
                              predictors.History(56)))

    twhist_tournament = predictors.Tournament(
        predictors.Tournament(predictors.TwiddledHistory(6),
                              predictors.TwiddledHistory(24)),
        predictors.Tournament(predictors.TwiddledHistory(48),
                              predictors.TwiddledHistory(56)))

    linear_tournament = predictors.Tournament(
        predictors.Linear(168), predictors.Linear(336))

    quarterfinal_1 = predictors.Tournament(
        predictors.RightNow(), predictors.SatCount())

    quarterfinal_2 = predictors.Tournament(hist_tournament, twhist_tournament)

    semifinal = predictors.Tournament(quarterfinal_1, quarterfinal_2)

    final = predictors.Tournament(semifinal, linear_tournament)

    for i, t in enumerate(training):
        final.train(t)
        prediction = final.predict(6 * 7 * 24 // 6)
    print ' '.join(map(str, prediction))
    sys.stdout.flush()
