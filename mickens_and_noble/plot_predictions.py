from __future__ import division

import cPickle as pickle
import gzip
import itertools
import sys

import matplotlib
import numpy as np
from matplotlib import pyplot as plt

matplotlib.rc('text', usetex=True)
matplotlib.rc('font', family='serif', size=20)
matplotlib.rc('legend', fontsize=20)
matplotlib.rc('figure', figsize=(8, 4.5))
plt.figure().subplots_adjust(top=0.93, bottom=0.2)

try:
    samples = np.loadtxt('samples')
    im = False
except IOError:
    samples = pickle.load(open('bins.pickle'))
    missing = list(set(xrange(1174)) - set(itertools.chain(*samples)))
    samples.append(missing)
    im = True

bins = {}
for i, nodes in enumerate(samples):
    for node in nodes:
        bins[node] = i

if im:
    assert sorted(bins) == range(1174)
else:
    assert len(bins) == 1000
    
results = [[0, 0, 0] for _ in xrange(11 if im else 10)]

def D():
    file = gzip.open('D.gz')
    for line in file:
        yield map(bool, map(float, line.strip().split()))

def logreg():
    file = gzip.open('predictions_D.gz')
    for line in file:
        yield [float(p) > 0.5 for p in line.strip().split()]

def tournament():
    file = open('tournament')
    for line in file:
        try:
            yield map(bool, map(int, line.strip().split()))
        except ValueError:
            #bug -- a print for debug remained here
            continue

def thebins():
    for k, v in sorted(bins.iteritems()):
        yield v
        
for (d, l, t, bin) in itertools.izip(D(), logreg(), tournament(), thebins()):
    
    assert len(d) == 24 * 7 * 6 // 6
    
    tot, log_score, tourn_score = results[bin]

    log_score += sum(v == p for v, p in zip(d, l))
    tourn_score += sum(v == p for v, p in zip(d, t))

    results[bin] = (tot + len(d), log_score, tourn_score)

print results

if len(sys.argv) > 1 and '--5b' in sys.argv:
    xs = [0.2 * i for i in range(5)]
    it = iter(results)
    results = [((d1 + d2), (l1 + l2), (t1 + t2))
               for (d1, l1, t1), (d2, l2, t2) in zip(it, it)]
    print len(xs), len(results)
else:
    xs = [0.1 * i for i in range(len(results))]

plt.plot(xs, [l / d for d, l, _ in results], '-x', label='Our method',
         lw=3, ms=12, mew=3)
plt.plot(xs, [t / d for d, _, t in results], '--+', label='MN',
         lw=3, ms=12, mew=3)
plt.xlabel('Availability class')
plt.ylabel('Accuracy')
plt.ylim(0.5, 1)
plt.legend(loc=0)
plt.grid()
plt.show()
