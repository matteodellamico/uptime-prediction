import collections
import itertools

import numpy as np
from scikits.talkbox import lpc
#from nmrglue import proc_lp
#from linpred import lpc

satcount = {
    (-2, 0): -2, (-2, 1): -1,
    (-1, 0): -2, (-1, 1): 1,
    (0, 0): -1, (0, 1): 1,
    (1, 0): -1, (1, 1): 2,
    (2, 0): 1, (2, 1): 2
}

satcount_3 = {
    (-4, 0): -4, (-4, 1): -3,
    (-3, 0): -4, (-3, 1): -2,
    (-2, 0): -3, (-2, 1): -1,
    (-1, 0): -2, (-1, 1): 1,
    (0, 0): -1, (0, 1): 1,
    (1, 0): -1, (1, 1): 2,
    (2, 0): 1, (2, 1): 3,
    (3, 0): 2, (3, 1): 4,
    (4, 0): 3, (4, 1): 4
}

class Predictor(object):
    
    def train_extend(self, vals):
        for v in vals:
            self.train(v)

class RightNow(Predictor):

    def __init__(self):
        self.last = 0
    
    def train(self, v):
        self.last = v

    def train_extend(self, vals):
        self.last = vals[-1]

    def predict(self, t):
        return [self.last] * t

class SatCount(Predictor):

    def __init__(self):
        self.val = 0
    
    def train(self, v):
        self.val = satcount[self.val, v]

    def predict(self, t):
        return [int(self.val > 0)] * t

class History(Predictor):

    def __init__(self, k):
        self.k = k
        self.state = (0,) * k
        self.counts = collections.defaultdict(itertools.repeat(0).next)

    def train(self, v):
        state = self.state
        self.counts[state] = satcount[self.counts[state], v]
        self.state = state[1:] + (v,)

    def predict(self, t):
        state = self.state
        counts = self.counts
        res = []
        for i in xrange(t):
            pred = int(counts[state] > 0)
            res.append(pred)
            state = state[1:] + (pred,)
        return res

class TwiddledHistory(History):
    
    def predict(self, t):
        state = self.state
        counts = self.counts

        res = []
        for i in xrange(t):
            tot = counts[state]
            for i in range(self.k):
                tw = list(state)
                tw[i] ^= 1
                tot += counts[tuple(tw)]
            pred = int(tot > 0)
            res.append(pred)
            state = state[1:] + (pred,)
        return res

class Linear(Predictor):

    def __init__(self, k):
        self.k = k
        self.signal = []

    def train(self, v):
        self.signal.append(v)

    def train_extend(self, vals):
        self.signal.extend(vals)

    def predict(self, t):
#        return proc_lp.lp(np.array(self.signal, 'float'), t,
#                          bad_roots=None, order=self.k)[-t:]

        k = self.k
        signal = self.signal

        try:
            coef = -lpc(np.array(signal), k)[0][:0:-1]
        except ValueError:
            return [0] * t
        state = signal[-k:]
        pred = []
        for i in xrange(t):
            new = int(np.dot(coef, state) > 0.5)
            pred.append(new)
            state = state[1:] + [new]
        return pred

class Tournament(Predictor):
    
    def __init__(self, pred1, pred2):
        self.pred1 = pred1
        self.pred2 = pred2
        self.scores = collections.defaultdict(itertools.repeat(0).next)
        self.current_item = 0
        self.predictions = collections.defaultdict(list)

    def train(self, v):
        
        pred = self.predictions
        scores = self.scores
        ci = self.current_item
        if ci in pred:
            for lookahead, p1, p2 in pred.pop(ci):
                if v == p1:
                    if v != p2:
                        scores[lookahead] = satcount_3[scores[lookahead], 0]
                elif v == p2:
                    scores[lookahead] = satcount_3[scores[lookahead], 1]
                    
        self.pred1.train(v)
        self.pred2.train(v)
        self.current_item += 1

    def predict(self, t):
        
        scores = self.scores
        pred = self.predictions
        ci = self.current_item

        p_left = self.pred1.predict(t)
        p_right = self.pred2.predict(t)

        res = []
        for lookahead, p1, p2 in itertools.izip(xrange(t), p_left, p_right):
            pred[ci + lookahead].append((lookahead, p1, p2))
            res.append(p1 if scores[lookahead] <= 0 else p2)
        return res
        
