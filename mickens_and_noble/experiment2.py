import gzip
import itertools
import operator
import sys

import predictors

A = gzip.open('A.gz')
B = gzip.open('B.gz')
C = gzip.open('C.gz')

def readvals(f):
    for l in f:
        yield map(int, map(float, l.strip().split()[::6]))

for i, lines in enumerate(itertools.izip(*map(readvals, [A, B, C]))):
    
    training = reduce(operator.add, lines)
    assert len(training) == 18 * 7 * 24 // 6

    hist_tournament = predictors.Tournament(
        predictors.Tournament(predictors.History(6),
                              predictors.History(24)),
        predictors.Tournament(predictors.History(48),
                              predictors.History(56)))

    twhist_tournament = predictors.Tournament(
        predictors.Tournament(predictors.TwiddledHistory(6),
                              predictors.TwiddledHistory(24)),
        predictors.Tournament(predictors.TwiddledHistory(48),
                              predictors.TwiddledHistory(56)))

    linear_tournament = predictors.Tournament(
        predictors.Linear(168), predictors.Linear(336))

    quarterfinal_1 = predictors.Tournament(
        predictors.RightNow(), predictors.SatCount())

    quarterfinal_2 = predictors.Tournament(hist_tournament, twhist_tournament)

    semifinal = predictors.Tournament(quarterfinal_1, quarterfinal_2)

    final = predictors.Tournament(semifinal, linear_tournament)

    for i, t in enumerate(training):
        final.train(t)
        prediction = final.predict(6 * 7 * 24 // 6)
    print ' '.join(map(str, prediction))
    sys.stdout.flush()
