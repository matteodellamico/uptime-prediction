from __future__ import division

import cPickle as pickle
import gzip
import itertools
import sys

import numpy as np

try:
    samples = np.loadtxt('samples')
    im = False
except IOError:
    samples = pickle.load(open('bins.pickle'))
    missing = list(set(xrange(1174)) - set(itertools.chain(*samples)))
    samples.append(missing)
    im = True

bins = {}
for i, nodes in enumerate(samples):
    for node in nodes:
        bins[node] = i

if im:
    assert sorted(bins) == range(1174)
else:
    assert len(bins) == 1000
    
for i, line in enumerate(sys.stdin):
    if i not in bins:
        continue
    print ' '.join(line.strip().split()[::6])
