import numpy as np
import scipy.linalg

def lpc(signal, p):
    l = len(signal)
    r = np.correlate(signal, signal, 'full')[l - 1: l + p]
    R = scipy.linalg.toeplitz(r[:-1])
    return scipy.linalg.solve(R, r[1:])
    
