import gzip
import itertools
import operator

import predictors

A = gzip.open('A.gz')
B = gzip.open('B.gz')
C = gzip.open('C.gz')

def readvals(f):
    for l in f:
        yield map(int, map(float, l.strip().split()))

preds = [('right_now', lambda: predictors.RightNow()),
         ('sat_count', lambda: predictors.SatCount()),
         ('history_6', lambda: predictors.History(6)),
         ('history_24', lambda: predictors.History(24)),
         ('history_48', lambda: predictors.History(48)),
         ('history_56', lambda: predictors.History(56)),
         ('tw_history_6', lambda: predictors.TwiddledHistory(6)),
         ('tw_history_24', lambda: predictors.TwiddledHistory(24)),
         ('tw_history_48', lambda: predictors.TwiddledHistory(48)),
         ('tw_history_56', lambda: predictors.TwiddledHistory(56)),
         ('linear_168', lambda: predictors.Linear(168)),
         ('linear_336', lambda: predictors.Linear(336))]


output_files = [gzip.open('mn/{}.gz'.format(pname), 'w') for pname, _ in preds]

for i, lines in enumerate(itertools.izip(*map(readvals, [A, B, C]))):
    training = reduce(operator.add, lines)
    print i
    assert len(training) == 18 * 7 * 24

    for (pname, factory), outfile in itertools.izip(preds, output_files):
        pred = factory()
        pred.train_extend(training)
        prediction = pred.predict(6 * 7 * 24)
        print >> outfile, ' '.join(map(str, prediction))
    
